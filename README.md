API COMPANIES PHONE
===========================

Installation
=====================

Pour installer l'api, tout d'abord clonnez le repository
```
git clone https://osakout@bitbucket.org/osakout/enterprise-details-october.git
```

Accédez au dossier source
```
cd enterprise-details-october
```

Installez les dépendances
```
npm i
```

Lancer le script de lancement
```
npm start
```

Vous êtes prêt à utiliser l'API


Endpoints
======================

***[GET] /api/company.json***
-------------

***Params*** : 
```
"query": "october"
```
***OR***
```
"query": "80426417400017"
```


***Response*** :

**Success** :
```
{
    "data": {
        "payload": [
            {
                "value": "03 21 27 60 68"
            },
            {
                "value": "03 21 02 96 79"
            }
        ]
    },
    "meta": {
        "code": "OK",
        "status": 200,
        "title": "Success",
        "detail": "Request Success"
    }
}
```

```
{
    "data": "No Phone Provided",
    "meta": {
        "code": "OK",
        "status": 200,
        "title": "Success",
        "detail": "Request Success"
    }
}
```

**Error** :
```
{
    "data": "Params query is null",
    "meta": {
        "code": "UNAUTHORIZED",
        "status": 401,
        "title": "Query",
        "detail": "Please add param Query"
    }
}
```

***/!\\*** L'api pour récupérer les infos des entreprise est soumise à une limite mensuel d'utilisation (25 crédits. 1 appel = 1 crédit), étant donné que je fais 2 appels à l'api pour récupérer le numéro de téléphone des entreprises, je consomme donc 2 crédits. 
Pour l'utiliser j'ai du créer 2 comptes chez eux pour mes test, et un 3ème compte pour que vous puissiez tester, vous aurez donc 12 tests à effectuer.