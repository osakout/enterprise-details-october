const logger = require('../../log/logger');
const axios = require('axios');
const addr = "https://societeinfo.com/app/rest/api/v2";

class CompanyRepository {

  async autoComplete(myQuery) {
    try {
      const company = await axios.get(`${addr}/companies.json/autocomplete`, {
        headers: {
          'X-API-KEY': process.env.SECRET_TOKEN
        },
        params: {
          query: myQuery
        },
      });
      logger.info(`Auto Complete success : ${JSON.stringify(company.data.result[0])}`);
      return company.data.result[0];
    } catch (error) {
      logger.error(`Erro while getting company in autoComplete() in [companyRepository.js] : ${error}`)
      return error;
    }
  }

  async getCompany(id, name) {
    try {
      const company = await axios.get(`${addr}/company.json`, {
        headers: {
          'X-API-KEY': process.env.SECRET_TOKEN
        },
        params: {
          'id': id,
          'name': name
        },
      });
      logger.info(`Get Company success : ${JSON.stringify(company.data)}`);
      return company.data.result.contacts.phones;
    } catch (error) {
      logger.error(`Erro while getting company in getCompany() in [companyRepository.js] : ${error}`);
      return error;
    }
  }


}

const companyRepository = new CompanyRepository();

module.exports = companyRepository;