const companyRepository = require('./companyRepository');
const logger = require('../../log/logger');

class CompanyAdapter {

  async companyPhone(myQuery) {
    try {
      const companyDetails = await companyRepository.autoComplete(myQuery);
      const { id, name } = companyDetails;
      const getCompany = await companyRepository.getCompany(id, name);
      logger.info(`Get Company success : ${JSON.stringify(getCompany)} in companyPhone() in companyAdapter.js`);
      return getCompany;
    } catch (error) {
      logger.error(`Erro while getting company in companyPhone() in [companyAdapter.js] : ${error}`);
      return error;
    }
  }
}

const companyAdapter = new CompanyAdapter();

module.exports = companyAdapter;