const express = require('express');
const Status = require('../helpers/api-status');
const { respond } = require('../utils/index');

const companyAdapter = require('../v1/company/companyAdapter');

const router = express.Router();

/* api landing. */
router.get('/', (req, res) => {
  respond(res, { data: {}, meta: Status.WELCOME },
    Status.WELCOME.status);
});

router.get('/company.json', (req, res) => {
  const { query } = req.query;

  if (!query) {
    respond(res, { data: 'Params query is null', meta: Status.UNAUTHORIZED },
      Status.UNAUTHORIZED.status);
  } else {
  companyAdapter.companyPhone(query).then(payload => {

    if (payload.length === 0) {
      respond(res, { data: 'No Phone Provided', meta: Status.OK },
        Status.OK.status);
    } else {
      respond(res, { data: {payload}, meta: Status.OK },
        Status.OK.status);
    }
  }).catch(error => {
    respond(res, { data: {error}, meta: Status.INTERNAL_SERVER_ERROR },
      Status.INTERNAL_SERVER_ERROR.status);
  });
}
});

module.exports = router;
