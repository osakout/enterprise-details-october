require('./config/config.js');

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser');
const winston = require('winston');
const cors = require('cors');
const logger = require('./app/log/logger');
const landing = require('./app/routes');
const api = require('./app/routes/api');

const app = express();

// Set CORS(Cross-origin resource sharing) options
const corsOptions = {
  origin: '*',
  allowedHeaders: ['Content-Type', 'Authorization'],
  optionsSuccessStatus: 200,
};

// Set logging level
winston.level = process.env.LOG_LEVEL;

app.use(bodyParser.json());

app.use(bodyParser.text());

app.use(validator({
  customValidators: {
  },
  customSanitizers: {
  }
}));

app.use(cors(corsOptions));

app.use('/', landing);
app.use('/api', api);

process.on('uncaughtException', (err) => {
  logger.info('crit', err.stack);
});

app.listen(process.env.PORT || process.env.LISTENER_PORT, () => {
  logger.info(`App is listening on http://localhost:${process.env.PORT || process.env.LISTENER_PORT}/`);
});
